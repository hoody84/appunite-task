import axios from 'axios';
const apiKey = '44ed29425a4e47bbb75242aa4ab31b3c';
const baseURL = `https://newsapi.org/v2/everything?language=en&apiKey=${apiKey}`;
const instance = axios.create({baseURL});

export default instance;