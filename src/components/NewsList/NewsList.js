import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import axios from '../../axios';
import NewsItem from '../NewsItem/NewsItem';
import Loader from '../Loader/Loader';
import { setCurrentPage, setItems, clearItems } from '../../actions/actions';
import styles from './NewsList.css';
import Filters from '../Filters/Filters';

const NewsList = (props) => {
    console.log(props);
    const [isLoading, setLoading] = useState(false);
    const [totalResults, setTotalResults] = useState(0);
    const getNews = (page, q = props.currentTopic || 'tech', reload = false, sortBy = 'popularity') => {
        setLoading(true);
        axios.get('', {
            params: {
                q,
                page,
                sortBy
            }
        }).then(function (response) {
            if (response.data.status === 'ok') {
                if (reload) {
                    props.clearItems();
                } else {
                    props.onPageLoaded(props.currentPage + 1);
                }
                setTotalResults(response.data.totalResults);
                props.onNewsLoaded(response.data.articles);
            }
            setLoading(false);
        })
        .catch(function (error) {
            console.log(error);
        })
        .finally(function () {
            setLoading(false);
        });
    }
    useEffect(() => {
        getNews(1);
    }, []);
    return (
        <div className="newsListWraper">
            <h1>Articles</h1>
            {totalResults > 0 && <Filters getNews={getNews} />}
            <div className="newsList">
                {props.items ? props.items.map((item, i) => <NewsItem data={item} key={i} />) : <Loader />}
            </div>
            {isLoading && <Loader />}
            {!isLoading && props.items.length < totalResults && <span onClick={() => { getNews(props.currentPage) }} className="button-secondary button--wide display-block text-center cnt">Load more</span>}
        </div>
    );
};

const mapStateToProps = state => ({
    currentPage: state.currentPage,
    items: state.items,
    currentTopic: state.currentTopic
});

const mapDispatchToProps = dispatch => {
    return {
        onPageLoaded: (currentPage) => dispatch(setCurrentPage(currentPage)),
        onNewsLoaded: (news) => dispatch(setItems(news)),
        clearItems: () => dispatch(clearItems())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);