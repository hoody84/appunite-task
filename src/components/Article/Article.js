import React from 'react';
import moment from 'moment';

const Article = (props) => {
    const article = props.location.state;
    if (!article) {
        window.location.href = '/';
    }
    return (
        <div className="news news--full">
            <h1>Article title</h1>
            <span className="news__backlink link" onClick={props.history.goBack}>Return to articles list</span>
            <div className="news__image" style={{ backgroundImage: `url(${article.urlToImage})`}}></div>
            <div className="news__inner cnt">
                <ul className="news__info">
                    <li className="news__date">{moment(article.publishedAt).format('ll')}</li>
                    {article.author && <li className="news__author">{article.author}</li>}
                    {article.source.name && <li className="news__source">{article.source.name}</li>}
                </ul>
                <h6 className="news__title">{article.title}</h6>
                <p className="news__excerpt">{article.content}</p>
                <a href={article.url} target="_blank" className="button text-center">Go To Source</a>
            </div>
        </div>
    );
};

export default Article;