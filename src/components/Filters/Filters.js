import React from 'react';
import styles from './Filters.css';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { connect } from 'react-redux';
import { setCurrentTopic, setSortOrder } from '../../actions/actions';

const Filters = (props) => {
    const topicOptions = [
        {
            value: 'tech',
            label: 'Tech'
        }, {
            value: 'travel',
            label: 'Travel'
        }, {
            value: 'politics',
            label: 'Politics'
        }, {
            value: 'sport',
            label: 'Sport'
        }
    ];
    const sortOrderOptions = [{
            value: 'popularity',
            label: 'Popularity'
        }, {
            value: 'publishedAt',
            label: 'Publication Date'
        }
    ]
    const changeTopic = (e) => {
        props.onSetCurrentTopic(e.value);
        props.getNews(1, e.value, true);
    };
    const changeSortOrder = (e) => {
        props.onSetSortOrder(e.value);
        props.getNews(1, props.currentTopic, true, e.value);
    };
    return (
        <div className="filters">
            <Dropdown
                options={topicOptions}
                onChange={changeTopic}
                value={props.currentTopic}
                placeholder={topicOptions[0]}
            />
            <Dropdown
                options={sortOrderOptions}
                onChange={changeSortOrder}
                value={props.sortOrder || sortOrderOptions[0]}
                placeholder={sortOrderOptions[0]}
            />
        </div>
    );
};

const mapStateToProps = state => ({
    currentTopic: state.currentTopic,
    sortOrder: state.sortOrder
});

const mapDispatchToProps = dispatch => {
    return {
        onSetCurrentTopic: (topic) => dispatch(setCurrentTopic(topic)),
        onSetSortOrder: (topic) => dispatch(setSortOrder(topic))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Filters);