import React from 'react';
import styles from './Loader.css';

const Loader = () => {
    return (
        <div className="loader cnt">loading</div>
    );
};

export default Loader;