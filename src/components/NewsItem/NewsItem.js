import React from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import slug from 'slug';
import style from './NewsItem.css';
import isUrl from 'is-valid-http-url';

const NewsItem = (props) => {
    const article = props.data || getItem(article.title);
    const articleSourceName = article.source.name;
    const articleContent = article.content;
    const excerptWordsCount = 30;
    const isValidURL = (str) => {
        var a = document.createElement('a');
        a.href = str;
        return (a.host && a.host != window.location.host);
    };
    return (
        <div className="news">
            <div className="news__content">
                <div className={`news__image ${!article.urlToImage ? 'news__image--blank' : null}`} style={{ backgroundImage: `url(${article.urlToImage})`}} > </div>
                <ul className="news__info">
                    <li className="news__date">{moment(article.publishedAt).format('ll')}</li>
                    {article.author && <li className="news__author">{article.author}</li>}
                    {articleSourceName && <li className="news__source">
                        {isUrl(`http://${articleSourceName}`) ? <a href={`http://${articleSourceName}`}>{articleSourceName}</a> : articleSourceName}
                    </li>}
                </ul>
                <h6 className="news__title">{article.title}</h6>
                {article.content && <p className="news__excerpt">{articleContent.split(' ', excerptWordsCount).join(' ')}...</p>}
                <Link to={{
                    pathname: `/article/${slug(article.title)}`,
                    state: article
                    }} className="button full-width display-block text-center">Read More</Link>
            </div>
        </div>
    );
};

export default NewsItem;