import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import NewsList from './components/NewsList/NewsList';
import Article from './components/Article/Article';
import normalize from './normalize.css';
import styles from './App.css';

const App = () => {
    return (
        <div className="main">
            <Router>
                <Switch>
                    <Route path="/" exact component={NewsList} />
                    <Route path="/article/:id" component={Article} />
                </Switch>
            </Router>
        </div>
    );
};

export default App;