export const SET_NEWS = 'SET_NEWS';
export const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';
export const SET_CURRENT_TOPIC = 'SET_CURRENT_TOPIC';
export const SET_SORT_ORDER = 'SET_SORT_ORDER';
export const SET_DATE = 'SET_DATE';
export const CLEAR_FILTERS = 'CLEAR_FILTERS';
export const CLEAR_ITEMS = 'CLEAR_ITEMS';

export const setCurrentPage = (currentPage) => {
    return {
        type: SET_CURRENT_PAGE,
        currentPage
    }
}

export const setItems = (items) => {
    return {
        type: SET_NEWS,
        items
    }
}

export const clearItems = () => {
    return {
        type: CLEAR_ITEMS
    }
}

export const setCurrentTopic = (currentTopic) => {
    return {
        type: SET_CURRENT_TOPIC,
        currentTopic
    }
}

export const setSortOrder = (sortOrder) => {
    return {
        type: SET_SORT_ORDER,
        sortOrder
    }
}