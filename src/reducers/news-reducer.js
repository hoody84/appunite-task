import {
    SET_NEWS,
    SET_CURRENT_PAGE,
    SET_SORT_ORDER,
    SET_DATE,
    CLEAR_ITEMS,
    SET_CURRENT_TOPIC
} from '../actions/actions';
const defaultState = {
    currentPage: 1,
    items: [],
    currentTopic: 'tech',
    sortOrder: 'popularity'
}
export const reducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_NEWS:
            return {
                ...state,
                items: [...state.items, ...action.items]
            };
        case SET_CURRENT_PAGE:
            return {
                ...state,
                currentPage: action.currentPage
            }
        case CLEAR_ITEMS:
            return {
                ...state,
                items: []
            }
        case SET_CURRENT_TOPIC:
            return {
                ...state,
                currentTopic: action.currentTopic
            }
        case SET_SORT_ORDER:
            return {
                ...state,
                sortOrder: action.sortOrder
            }
        default:
            return state;
    }
}
